<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class loginController extends Controller
{
    public function indexAction()
    {
        return $this->render('TestBundle:login:login.html.twig');
    }
}
