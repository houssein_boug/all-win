<?php

namespace MyApp\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvenementForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $mydate= new \DateTime();
        $year=intval( $mydate->format('y'));


        $builder
            //->add('id')
            ->add('titre')
            ->add('adresse')
            ->add('dateDebut',BirthdayType::class,array('years'=>range($year,$year+30)))
            ->add('dateFin',BirthdayType::class,array('years'=>range($year,$year+30)))
            ->add('description', TextareaType::class,array('attr' => array('rows'=>'8') ) )
            ->add('photo',FileType::class,array('label'=>'Photo','required'=> false) )
            ->setMethod('POST');
            //->add('ajouter',SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'user_bundle_evenement_form';
    }
}
