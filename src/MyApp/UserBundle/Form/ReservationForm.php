<?php

namespace MyApp\UserBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use MyApp\UserBundle\Entity\Match;

class ReservationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idUser',EntityType::class,array(
                'class'=>'MyApp\UserBundle\Entity\User',
                'choice_label'=>'nom',
                'multiple'=>false,
            ))
            ->add('idMatch')
            /*    ,EntityType::class,array(
                'class'=>'MyApp\UserBundle\Entity\Match',
                'multiple'=>false,
            ))*/
            ->add('nbreTicketReserve')
            ->setMethod('GET')
            ->add('Ajouter',SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'user_bundle_reservation_form';
    }
}
