<?php

namespace MyApp\UserBundle\Form;

use Doctrine\ORM\Query\AST\Functions\CurrentDateFunction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date')
            ->add('nbrTicketsDispo')
            ->add('idJoueur2')
            ->add('idStade')
            ->add('idJoueur1')
            ->add('idArbitre')        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyApp\UserBundle\Entity\Match'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'myapp_userbundle_match';
    }


}
