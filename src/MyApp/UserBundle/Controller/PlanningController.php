<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Planning;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MyApp\UserBundle\Form\PlanningForm;
use Symfony\Component\HttpFoundation\Request;

class PlanningController extends Controller
{
    public function ajoutPlanningAction(Request $request){

        $username =(string) $this->getUser();
        if ($request->isXmlHttpRequest()) {
            $end = $request->request->get('end');
            $start = $request->request->get('start');
            $title = $request->request->get('title');
            var_dump($start);}
        //methode d'ajout avec le form
        $planning= new Planning();
        $form=$this->createForm(PlanningForm::class,$planning);
        $form->handleRequest($request);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($planning);
            $em->flush();
            return $this->redirectToRoute('afficher_Planning');

        }

        return $this->render('UserBundle:Planning:ajoutPlanning.html.twig',
            array(
                'formes'=>$form->createView(),
                'username'=>$username

            ));

    }

    public function afficherPlanningAction(){

        $username =(string) $this->getUser();

        $em=$this->getDoctrine()->getManager();
        $plannings=$em->getRepository("UserBundle:Planning")->findAll();
        return $this->Render('UserBundle:Planning:afficherPlanning.html.twig',
            array('plannings'=>$plannings,'username'=>$username));

    }

    public  function deletePlanningAction($id) {

        $em=$this->getDoctrine()->getManager();
        $planning=$em->getRepository('UserBundle:Planning')->find($id);
        $em->remove($planning);
        $em->flush();
        return $this->redirectToRoute('afficher_Planning');

    }
    public  function  updatePlanningAction($id, Request $request) {
        $username =(string) $this->getUser();


        $em=$this->getDoctrine()->getManager();
        $planning=$em->getRepository('UserBundle:Planning')->find($id);
        $Form=$this->createForm(PlanningForm::class,$planning);
        $Form->handleRequest($request);
        if($Form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($planning);
            $em->flush();
            return $this->redirectToRoute('afficher_Planning');
        }

        return $this->render('UserBundle:Planning:updatePlanning.html.twig',
            array(
                'formes'=>$Form->createView(),
                'username'=>$username

            ));

    }
}
