<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Coupon;
use MyApp\UserBundle\Entity\Joueur;
use MyApp\UserBundle\Entity\Match;
use MyApp\UserBundle\Entity\Paris;
use MyApp\UserBundle\Entity\Stade;
use MyApp\UserBundle\Entity\User;
use MyApp\UserBundle\UserBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;

class PariController extends Controller
{
    public function generateTestAction()
    {
       // $stade=new Stade();
       // $stade->setNom('stadex');
       // $stade->setCapacite(2000);
        $em=$this->getDoctrine()->getManager();
        //$em->persist($stade);
        // $em->flush();

        for($i=3000;$i<=3100;$i++)
        {
            $nom= "Joueur".$i;
            $joueur=new Joueur();
            $joueur->setNom($nom);
            $joueur->setPrenom("BenJoueur");
            $joueur->setEmail($nom."@gmail.com");
            $joueur->setRang($i);
            $joueur->setAdresse("Tunisie");
            $joueur->setDateNaissance(new \DateTime());
            $joueur->setSex("homme");
            $joueur->setNationalite("Tunisien");

            $em=$this->getDoctrine()->getManager();
            $em->persist($joueur); //insert into
            $em->flush();

            if (($i % 2) == 0 && 1==2)  // alors on ajoute un match
            {
                $match=new Match();
                $joueur =new Joueur();

                $joueur=$em->getRepository('UserBundle:Joueur')->findOneBy(array('nom'=>$nom));
                $match->setIdJoueur2($joueur);
                $y=$i-1;
                $nom="Joueur".$y;
                $joueur=$em->getRepository('UserBundle:Joueur')->findOneBy(array('nom'=>$nom));
                $match->setIdJoueur1($joueur);

                $users = $em->getRepository('UserBundle:User')->findAll();
                $arbitre=$users[0];
                $match->setIdArbitre($arbitre);

                $stade=$em->getRepository('UserBundle:Stade')->findOneBy(array('nom'=>'stadex'));
                $match->setIdStade($stade);
                $match->setDate(new \DateTime());
                $match->setNbrTicketsDispo(1000);

                $dd= $match->getDate();
                $dd->setDate(2017,1 ,$i%30);
                $match->setDate($dd);


                $em->persist($match); //insert into
                $em->flush();

                $this->forward('UserBundle:Pari:generatePari ');

            }

        }

        return $this->redirectToRoute('acceuil_homepage');
    }

    public function generatePariAction()
    {
        $em = $this->getDoctrine()->getManager();
        $existe = $em->getRepository('UserBundle:Paris')->findOneBy(array('status'=>0));
        if ($existe !== null)
            return new Response();



        $pari=new Paris();
        $dd= new \DateTime();
        $df= new \DateTime();

      //  $df=$dd;
        date_add($dd, date_interval_create_from_date_string('1 days'));
        date_add($df, date_interval_create_from_date_string('40 days'));
        var_dump($dd);
        var_dump($df);




        $query = $em->createQuery(
            'SELECT m
            FROM UserBundle:Match m
            WHERE  m.date > :dd and m.date< :df '

        )->setParameter('dd',$dd)
        ->setParameter('df',$df);

        $matchs = $query->getResult();

        //var_dump($c);
       // return new Response();
        var_dump(count($matchs));
        $n=count($matchs);
        if ($n >= 16 )
        {
            for($i=0;$i<12;$i++)
            {
                srand();
                $x = rand(0,$n-1);
                //var_dump($x);
                $m=array_splice($matchs,$x,1);
                //var_dump(count($m));
                var_dump($m[0]->getId());
                $n--;
                $pari->setIdMatch($i+1,$m[0]);
            }

            $pari->setDate( new \DateTime());
            $pari->setStatus(0);

            $em->persist($pari);
            $em->flush();

            return $this->redirectToRoute('acceuil_homepage');
        }

        return new Response();

    }

    // fonction de retour type booleen qui verifie si l'instance du classe user passer en parametre possede le role client ou non

    public function isClient($user)
    {
        if ($user instanceof User)
        {
            $roles =$user->getRoles();
            foreach ($roles as $r)
            {
                if ($r == 'ROLE_CLIENT')
                {
                    return true ;
                }
            }

        }

        return false;
    }

   public function AfficherPariAction()
   {


       $em = $this->getDoctrine()->getManager();

       $pari =  $em->getRepository('UserBundle:Paris')->findOneBy(array('status'=>0));



       $user = $this->getUser();

       $isUser = $this->isClient($user)  ;

       if ($pari instanceof Paris )
       {


            if ($isUser)
            {
                $today = new  \DateTime();
                //var_dump($today);
                $dd= $pari->getIdMatch1()->getDate();
                for($i=2;$i<13;$i++)
                {
                    if ( $pari->getIdMatch($i)->getDate() < $dd )
                    {
                        $dd= $pari->getIdMatch($i)->getDate();
                    }
                }

                //var_dump($dd);

                if( $dd <= $today )
                {
                    $pari->setStatus(1);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($pari);
                    $em->flush();
                   //$this->AfficherPariAction();
                    //return new Response();
                    $em= $this->getDoctrine()->getManager();
                    $coupons=$em->getRepository('UserBundle:Coupon')->findBy(array('idUser'=>$user));
                    return $this->render('UserBundle:User:jouerPari.html.twig',array('flag'=>'no','pari'=> $pari,'coupons'=>$coupons));

                }

                $interval1 = date_diff($dd, $pari->getDate(),true)->days ;
                $interval2 = date_diff( $dd, $today,true)->days ;
                $loadin =  intval((($interval1-$interval2) / $interval1)*100)  ;
                $em= $this->getDoctrine()->getManager();
                $coupons=$em->getRepository('UserBundle:Coupon')->findBy(array('idUser'=>$user));

                return $this->render('UserBundle:User:jouerPari.html.twig',array('flag'=>'yes','pari'=> $pari,'loadin'=>$loadin,'dd'=>$dd,'coupons'=>$coupons));
            }

           $em= $this->getDoctrine()->getManager();
           $coupons=$em->getRepository('UserBundle:Coupon')->findBy(array('idUser'=>$user));
            //else
                 return $this->render('UserBundle:acceuil:pari.html.twig',array('flag'=>'yes','pari'=> $pari,'coupons'=>$coupons));
       }

       $em= $this->getDoctrine()->getManager();
       $coupons=$em->getRepository('UserBundle:Coupon')->findBy(array('idUser'=>$user));
       if ($isUser)
       {

           return $this->render('UserBundle:User:jouerPari.html.twig',array('flag'=>'no','coupons'=>$coupons));
       }

       //else
        return $this->render('UserBundle:acceuil:pari.html.twig',array('flag'=>'no','coupons'=>$coupons));

   }

   public function verifierReponse($reponse)
   {
       if (strlen($reponse)==12)
       {
           for ( $i=0;$i<12;$i++)
           {
               if($reponse[$i] != '1' and $reponse[$i]!='2')
                   return false ;
           }
           return true ;
       }
       return false;
   }


   public function ajouterCouponAction(Request $request)
   {

       $resp = new Response();
       $cont = "";


       $reponse = $request->get('reponse');
       if ( $this->verifierReponse($reponse) )
       {
           $user= $this->getUser();
           if( $this->isClient($user))
           {
               $em=$this->getDoctrine()->getManager();
               $pari=$em->getRepository('UserBundle:Paris')->findOneBy(array('status'=>'0'));

               $jetons =$user->getJetons();
               if ($jetons<10 )
               {
                   $cont= "no";
                   $resp->setContent($cont);
                   return $resp;
               }

               $coupon = new Coupon();

               $coupon->setIdParis($pari);
               $coupon->setIdUser($user);
               $coupon->setDate(new \DateTime());
               $coupon->setReponse($reponse);
               $coupon->setStatus(0);

               $em->persist($coupon);
               $em->flush();
               $jetons=$jetons -10 ;
               $user->setJetons($jetons);
               $em->persist($user);
               $em->flush();


               $cont= "<div class='panel panel-green'><div class='panel-heading'  style='cursor: pointer;'  onclick='showme(".($coupon->getId()*5).")'  >";
               $cont=$cont.$coupon->getDate()->format('Y-m-d')." :active </div> ";
               $cont=$cont."<div id='b".$coupon->getId()*5 ."'class='panel-body' style='display:none;'>";

               $pari = $coupon->getIdParis();
               for($i=1;$i<13;$i++)
               {
                   $cont=$cont.$pari->getIdMatch($i)->getIdJoueur1()->getNom()." Vs ".$pari->getIdMatch($i)->getIdJoueur2()->getNom()." :".$coupon->getReponse()[$i-1]."<br>";
               }

               $cont=$cont."</div> </div> ";



                  $resp->setContent($cont);
               return $resp;

           }
       }
       return new Response(null) ;

   }
}
