<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Reservation;
use MyApp\UserBundle\Form\ReservationForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReservationController extends Controller
{
    public function ajoutAction(Request $request){

        $username =(string) $this->getUser();
        //methode d'ajout avec le form
        $reservation= new Reservation();
        $form=$this->createForm(ReservationForm::class,$reservation);
        $form->handleRequest($request);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($reservation);
            $em->flush();
            return $this->redirectToRoute('afficher_Reservation');

        }

        return $this->render('UserBundle:Reservation:ajoutReservation.html.twig',
            array(
                'formes'=>$form->createView(),
                'username'=>$username

            ));

    }

    public function afficherReservationAction(){

        $username =(string) $this->getUser();

        $em=$this->getDoctrine()->getManager();
        $reservations=$em->getRepository("UserBundle:Reservation")->findAll();
        return $this->Render('UserBundle:Reservation:afficherReservation.html.twig',
            array('reservations'=>$reservations,'username'=>$username));

    }

    public  function DeleteAction($id) {

        $em=$this->getDoctrine()->getManager();
        $reservation=$em->getRepository('UserBundle:Reservation')->find($id);
        $em->remove($reservation);
        $em->flush();
        return $this->redirectToRoute('afficher_Reservation');

    }
    public  function  updateAction($id, Request $request) {
        $username =(string) $this->getUser();


        $em=$this->getDoctrine()->getManager();
        $reservation=$em->getRepository('UserBundle:Reservation')->find($id);
        $Form=$this->createForm(ReservationForm::class,$reservation);
        $Form->handleRequest($request);
        if($Form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($reservation);
            $em->flush();
            return $this->redirectToRoute('afficher_Reservation');
        }

        return $this->render('UserBundle:Reservation:updateReservation.html.twig',
            array(
                'formes'=>$Form->createView(),
                'username'=>$username

            ));

    }
}
