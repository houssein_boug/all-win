<?php

namespace MyApp\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class mapController extends Controller
{
    public function showMapAction()
    {
        $Latitudes =36.898392;
        $Longitudes =10.189732;

        return $this->render('UserBundle:map:map.html.twig', array('Latitudes' => $Latitudes,'Longitudes'=>$Longitudes));
    }

}
