<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\CompteRenduMatch;
use MyApp\UserBundle\Entity\Match;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Match controller.
 *
 */
class MatchController extends Controller
{
    /**
     * Lists all match entities.
     *
     */
    public function indexAction()
    {
        $username =(string) $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $matches = $em->getRepository('UserBundle:Match')->findAll();

        return $this->render('@User/views/match/index.html.twig', array(
            'matches' => $matches,
            'username'=> $username
        ));
    }

    /**
     * Creates a new match entity.
     *
     */
    public function newAction(Request $request)
    {
        $username =(string) $this->getUser();
        $match = new Match();
        $form = $this->createForm('MyApp\UserBundle\Form\MatchForm', $match);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($match);
            $em->flush($match);

            return $this->redirectToRoute('match_show', array('id' => $match->getId()));
        }

        return $this->render('UserBundle:views/match:new.html.twig', array(
            'match' => $match,
            'form' => $form->createView(),
            'username'=> $username
        ));
    }

    /**
     * Finds and displays a match entity.
     *
     */
    public function showAction(Match $match)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($match);

        return $this->render('@User/views/match/show.html.twig', array(
            'match' => $match,
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Displays a form to edit an existing match entity.
     *
     */
    public function editAction(Request $request, Match $match)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($match);
        $editForm = $this->createForm('MyApp\UserBundle\Form\MatchForm', $match);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('match_edit', array('id' => $match->getId()));
        }

        return $this->render('@User/views/match/edit.html.twig', array(
            'match' => $match,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Deletes a match entity.
     *
     */
    public function deleteAction(Request $request, Match $match)
    {
        $flag="ok";
        $username =(string) $this->getUser();
        $form = $this->createDeleteForm($match);
        $form->handleRequest($request);
        $em= $this->getDoctrine()->getManager();
        $cr= $em->getRepository('UserBundle:CompteRenduMatch')->findOneBy(array('idMatch'=>$match->getId()));
        if ($cr instanceof CompteRenduMatch)
        {
            $flag="no";
            return $this->redirectToRoute('match_index');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($match);
            $em->flush($match);
        }

        return $this->redirectToRoute('match_index');
    }

    /**
     * Creates a form to delete a match entity.
     *
     * @param Match $match The match entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Match $match)
    {
        $username =(string) $this->getUser();
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('match_delete', array('id' => $match->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function rechercheAction(Request $request)
    {   $username =(string) $this->getUser();


        $em=$this->getDoctrine()->getManager();
        $motcle=$request->get('motcle');
        $listmatch=$em->getRepository('UserBundle:Match')->findOneBy(array('id'=>$motcle));

        return $this->render('UserBundle:views/match:recherche.html.twig',array('listmatch'=>$listmatch,'username'=> $username));

    }
}
