<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Competition controller.
 *
 */
class CompetitionController extends Controller
{
    /**
     * Lists all competition entities.
     *
     */
    public function indexAction()
    {   $username =(string) $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $competitions = $em->getRepository('UserBundle:Competition')->findAll();

        return $this->render('@User/views/competition/index.html.twig', array(
            'competitions' => $competitions,
            'username'=> $username
        ));
    }

    /**
     * Creates a new competition entity.
     *
     */
    public function newAction(Request $request)
    {   $flag="ok";
        $username =(string) $this->getUser();
        $competition = new Competition();
        $form = $this->createForm('MyApp\UserBundle\Form\CompetitionForm', $competition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();


           $datedeb = $competition->getDateDebut();
            $datefin = $competition->getDateFin();
            if($datedeb < $datefin) {
                $em->persist($competition);
                $em->flush($competition);
                return $this->redirectToRoute('competition_show', array('id' => $competition->getId()));

               // return new Response("date invalide");

           // return $this->redirectToRoute('competition_show', array('id' => $competition->getId()));
            }
            else{
                 return new Response("date invalide");
                }
        }

        return $this->render('@User/views/competition/new.html.twig', array(
            'competition' => $competition,
            'form' => $form->createView(),
            'username'=> $username
        ));
   }

    /**
     * Finds and displays a competition entity.
     *
     */
    public function showAction(Competition $competition)
    {   $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($competition);

        return $this->render('@User/views/competition/show.html.twig', array(
            'competition' => $competition,
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Displays a form to edit an existing competition entity.
     *
     */
    public function editAction(Request $request, Competition $competition)
    {   $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($competition);
        $editForm = $this->createForm('MyApp\UserBundle\Form\CompetitionForm', $competition);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competition_edit', array('id' => $competition->getId()));
        }

        return $this->render('@User/views/competition/edit.html.twig', array(
            'competition' => $competition,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Deletes a competition entity.
     *
     */
    public function deleteAction(Request $request, Competition $competition)
    {
        $form = $this->createDeleteForm($competition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($competition);
            $em->flush($competition);

        }

        return $this->redirectToRoute('competition_index');
    }

    /**
     * Creates a form to delete a competition entity.
     *
     * @param Competition $competition The competition entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Competition $competition)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('competition_delete', array('id' => $competition->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
