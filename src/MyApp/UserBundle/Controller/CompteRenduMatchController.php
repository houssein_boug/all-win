<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\CompteRenduMatch;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Compterendumatch controller.
 *
 */
class CompteRenduMatchController extends Controller
{
    /**
     * Lists all compteRenduMatch entities.
     *
     */
    public function indexAction()
    {   $username =(string) $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $compteRenduMatches = $em->getRepository('UserBundle:CompteRenduMatch')->findAll();

        return $this->render('@User/views/compterendumatch/index.html.twig', array(
            'compteRenduMatches' => $compteRenduMatches,
            'username'=> $username
        ));
    }

    /**
     * Creates a new compteRenduMatch entity.
     *
     */
    public function newAction(Request $request)
    {   $username =(string) $this->getUser();
        $compteRenduMatch = new Compterendumatch();
        $form = $this->createForm('MyApp\UserBundle\Form\CompteRenduMatchForm', $compteRenduMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($compteRenduMatch);
            $em->flush($compteRenduMatch);

            return $this->redirectToRoute('compterendumatch_show', array('id' => $compteRenduMatch->getId()));
        }

        return $this->render('@User/views/compterendumatch/new.html.twig', array(
            'compteRenduMatch' => $compteRenduMatch,
            'form' => $form->createView(),
            'username'=> $username
        ));
    }

    /**
     * Finds and displays a compteRenduMatch entity.
     *
     */
    public function showAction(CompteRenduMatch $compteRenduMatch)
    {   $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($compteRenduMatch);

        return $this->render('@User/views/compterendumatch/show.html.twig', array(
            'compteRenduMatch' => $compteRenduMatch,
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Displays a form to edit an existing compteRenduMatch entity.
     *
     */
    public function editAction(Request $request, CompteRenduMatch $compteRenduMatch)
    {   $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($compteRenduMatch);
        $editForm = $this->createForm('MyApp\UserBundle\Form\CompteRenduMatchForm', $compteRenduMatch);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('compterendumatch_edit', array('id' => $compteRenduMatch->getId()));
        }

        return $this->render('@User/views/compterendumatch/edit.html.twig', array(
            'compteRenduMatch' => $compteRenduMatch,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));
    }

    /**
     * Deletes a compteRenduMatch entity.
     *
     */
    public function deleteAction(Request $request, CompteRenduMatch $compteRenduMatch)
    {
        $form = $this->createDeleteForm($compteRenduMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($compteRenduMatch);
            $em->flush($compteRenduMatch);
        }

        return $this->redirectToRoute('compterendumatch_index');
    }

    /**
     * Creates a form to delete a compteRenduMatch entity.
     *
     * @param CompteRenduMatch $compteRenduMatch The compteRenduMatch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CompteRenduMatch $compteRenduMatch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('compterendumatch_delete', array('id' => $compteRenduMatch->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
