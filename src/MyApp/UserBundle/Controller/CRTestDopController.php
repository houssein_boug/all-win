<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\CRTestDop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Dompdf\Dompdf;
require_once 'dompdf/autoload.inc.php';

/**
 * Crtestdop controller.
 *
 */
class CRTestDopController extends Controller
{
    /**
     * Lists all cRTestDop entities.
     *
     */
    public function indexAction()
    {

        $username =(string) $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $cRTestDops = $em->getRepository('UserBundle:CRTestDop')->findAll();

        return $this->render('UserBundle:SuperAdmin:crtestdop.html.twig', array(
            'cRTestDops' => $cRTestDops,
            'username'=>$username
        ));
    }

    /**
     * Creates a new cRTestDop entity.
     *
     */
    public function newAction(Request $request)
    {

        $username =(string) $this->getUser();
        $cRTestDop = new Crtestdop();
        $form = $this->createForm('MyApp\UserBundle\Form\CRTestDopType', $cRTestDop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cRTestDop);
            $em->flush($cRTestDop);

            return $this->redirectToRoute('crtestdop_show', array('id' => $cRTestDop->getId(),
                'username'=>$username
            ));
        }



        return $this->render('UserBundle:crtestdop:new.html.twig', array(
            'cRTestDop' => $cRTestDop,
            'form' => $form->createView(),
            'username'=>$username
        ));
    }

    /**
     * Finds and displays a cRTestDop entity.
     *
     */
    public function showAction(CRTestDop $cRTestDop)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($cRTestDop);

        return $this->render('UserBundle:crtestdop:show.html.twig', array(
            'cRTestDop' => $cRTestDop,
            'delete_form' => $deleteForm->createView(),
            'username'=>$username
        ));
    }

    /**
     * Displays a form to edit an existing cRTestDop entity.
     *
     */
    public function editAction(Request $request, CRTestDop $cRTestDop)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($cRTestDop);
        $editForm = $this->createForm('MyApp\UserBundle\Form\CRTestDopType', $cRTestDop);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('crtestdop_edit', array('id' => $cRTestDop->getId()));
        }

        return $this->render('UserBundle:crtestdop:edit.html.twig', array(
            'cRTestDop' => $cRTestDop,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=>$username
        ));
    }

    /**
     * Deletes a cRTestDop entity.
     *
     */
    public function deleteAction(Request $request, CRTestDop $cRTestDop)
    {
        $username =(string) $this->getUser();
        $form = $this->createDeleteForm($cRTestDop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cRTestDop);
            $em->flush($cRTestDop);
        }

        return $this->redirectToRoute('crtestdop_index');
    }

    /**
     * Creates a form to delete a cRTestDop entity.
     *
     * @param CRTestDop $cRTestDop The cRTestDop entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CRTestDop $cRTestDop)
    {
        $username =(string) $this->getUser();
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('crtestdop_delete', array('id' => $cRTestDop->getId(),
                'username'=>$username
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function pdfAction($cRTestDop){
        $cRTestDop=new CRTestDop();



        $dompdf=new Dompdf();
        $dompdf->load_html('
        <h2 align="center" >conte rendu</h2>
         <table>
        <tbody>
            <tr>
                <th>Id :</th>
                <td>{{ cRTestDop.id }}</td>
            </tr>
            <tr>
                <th>Enonce :</th>
                <td>{{ cRTestDop.enonce }}</td>
            </tr>
            <tr>
                <th>Destinataire :</th>
                <td>{{ cRTestDop.destinataire }}</td>
            </tr>
            <tr>
                <th>Expediteur :</th>
                <td>{{ cRTestDop.expediteur }}</td>
            </tr>
        </tbody>
    </table>
        
        ');
        $dompdf->setPaper('A4','landscape');
        $dompdf->render('UserBundle:crtestdop:pdf.html.twig');
        $dompdf->stream('conte rendu',array('Attachement'=>0));
        return $this->redirectToRoute('afficher_pdf', array('d'=>0));

    }

}
