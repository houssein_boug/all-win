<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 12/21/2016
 * Time: 12:33 AM
 */

namespace MyApp\UserBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class mobileController extends Controller
{
    public function loginAction(Request $request)
    {
        $username =$request->get('username');
        $password =$request->get('password');


        $userManager=$this->get('fos_user.user_manager');
        $user=$userManager->findUserByUsername($username);
        if (!$user) {
            echo "invalid";
            return new Response(null);
        }

        $factory=$this->get('security.encoder_factory');
        $encoder=$factory->getEncoder($user);
        $bool= $encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt());
        $resultat = ($bool) ? "OK" : "invalid" ;
        echo $resultat;
        return new Response(null);
        //$token= new UsernamePasswordToken($user,$user->getPassword(),'main',$user->getRoles());
        //var_dump((string) $token);




        // $token = new UsernamePasswordToken($username, $password,'main');
        //$authToken = $this->authManager->authenticate($token);
        //var_dump((string)$authToken);

        /*
             $url=$this->generateUrl('fos_user_security_login');
        $r = Request::create($url,'POST', array(    '_username' => 'mahdi', //$username,
                                                       '_password' => 'mahdi', //$password,
                                                     '_csrf_token' => $csrf));
           $text='' ;
          try {
               $text = $r->send()->getBody();
            catch (\HttpException $ex) {
               echo $ex;
           }



           $response = $this->forward('FOSUserBundle:Security:check',
               array('_username' => 'mahdi', //$username,
                   '_password' => 'mahdi', //$password,
                   '_csrf_token' => $csrf));


           $user = $response->getContent();
           echo 'user:'.$user ;

   */


        // return $this->render('UserBundle::test.html.twig');


    }

    public function couponsAction(Request $request)
    {
        $username =$request->get('username');
        $userManager=$this->get('fos_user.user_manager');
        $user=$userManager->findUserByUsername($username);

        if (!$user) {
            echo "invalid";
            return new Response(null);
        }


        $em= $this->getDoctrine()->getManager();
        $coupons=$em->getRepository('UserBundle:Coupon')->findBy(array('idUser'=>$user));
        $result = [];
        foreach ($coupons as $c )
        {
            $c_array['id']=$c->getId()."";
            $c_array['date']=$c->getDate()->format('y-m-d');
            $c_array['reponse']=$c->getReponse();
            for ( $i=1;$i<=12;$i++)
            {
                $joueur1=$c->getIdParis()->getIdMatch($i)->getIdJoueur1();
                $joueur2=$c->getIdParis()->getIdMatch($i)->getIdJoueur2();
                $id1=$joueur1->getId();
                $id2=$joueur2->getId();

                $c_array['match'.$i]=array('joueur1'=>$joueur1->getNom(),'joueur2'=>$joueur2->getNom()) ;
            }


            $c_array['status']=$c->getStatus()."";
            array_push($result,$c_array);
        }
        echo json_encode($result);

        return new Response(null);

    }

    public function getEventAction()
    {
        $em= $this->getDoctrine()->getManager();
        $events=$em->getRepository('UserBundle:Evenement')->findAll();
        $result = [];
        foreach ($events as $e )
        {
            $e_array['id']=$e->getId()."";
            $e_array['titre']=$e->getTitre()."";
            $e_array['description']=$e->getDescription()."";
            $e_array['adresse']=$e->getAdresse()."";
            $e_array['dd']=$e->getDateDebut()->format('y-m-d');
            $e_array['df']=$e->getDateFin()->format('y-m-d');
           // $fs = new Filesystem();
            //$fs->chgrp($this->getParameter('events_photos_directory'),$e->getPhoto());


            $f =new File($this->getParameter('events_photos_directory').'/'.$e->getPhoto());
            //$e_array['photo']=json_encode($f);


           // $encoders = array(new XmlEncoder(), new JsonEncoder());
           // $normalizers = array(new ObjectNormalizer());

           // $serializer = new Serializer($normalizers, $encoders);
            //$jsonContent = $serializer->serialize($f, 'json');

            $e_array['photo']="http://localhost/AllWin/web/uploads/event_photos/".$f->getBasename();
            array_push($result,$e_array);
        }

        echo json_encode($result);
        return new Response(null);

    }




    public function loginjAction(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');


        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        if (!$user) {
            echo "invalid";

            //  return $this->redirect( "http://google.fr/?no*");
            return new Response();
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $bool = $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
        $ch =(string) $user->getUsername();
        $r = $user->getRoles()[0];
        $resultat = ($bool) ? $ch."+".$r : "invalid";
        echo $resultat;
        // return $this->redirect($resultat);
        return new Response();
    }

}