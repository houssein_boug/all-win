<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Joueur;
use MyApp\UserBundle\Form\JoueurForm;
use MyApp\UserBundle\Form\RechercheJoueurForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JoueurController extends Controller
{
    public function afficherJoueurAction(Request $request){

        $username =(string) $this->getUser();

        $em=$this->getDoctrine()->getManager();
        $joueurs=$em->getRepository("UserBundle:Joueur")->findAll();
        return $this->Render('UserBundle:Joueur:afficherJoueur.html.twig',
            array('joueurs'=>$joueurs,'username'=>$username));
    }


    public  function DeleteAction($id) {

        $em=$this->getDoctrine()->getManager();
        $joueur=$em->getRepository('UserBundle:Joueur')->find($id);
        $em->remove($joueur);
        $em->flush();
        return $this->redirectToRoute('afficher_Joueur');

    }



    public  function  updateAction($id, Request $request) {
        $username =(string) $this->getUser();


        $em=$this->getDoctrine()->getManager();
        $joueur=$em->getRepository('UserBundle:Joueur')->find($id);
        $Form=$this->createForm(JoueurForm::class,$joueur);
        $Form->handleRequest($request);
        if($Form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($joueur);
            $em->flush();
            return $this->redirectToRoute('afficher_Joueur');
        }

        return $this->render('UserBundle:Joueur:update.html.twig',
            array(
                'formes'=>$Form->createView(),
                'username'=>$username

            ));

    }

    public function  ajoutAction(Request $request){

        $username =(string) $this->getUser();
        //methode d'ajout avec le form
        $joueur=new Joueur();
        $Form=$this->createForm(JoueurForm::class,$joueur);
        $Form->handleRequest($request);

        if($Form->isValid()){
//            if ( $this->JoueurExist($joueur)== true){
//                return $this->redirectToRoute('acceuil_homepage');
//            }
//            $validator=$this->get('validator');
//            $errors=$validator->validate($joueur);
//            if(count($errors)>0){
//
//                return $this->render('UserBundle:Joueur:validateJoueur.html.twig', array(
//                    'errors' => $errors,
//                ));

//                $errorString=(String) $errors;
//                return new Response($errorString);

            $em=$this->getDoctrine()->getManager();
            $em->persist($joueur);
            $em->flush();
            return $this->redirectToRoute('afficher_Joueur');
        }

        return $this->render('UserBundle:Joueur:ajout.html.twig',
            array(
                'formes'=>$Form->createView(),
                'username'=>$username

            ));

    }

    public function JoueurExist(Joueur $joueur1){

        $em=$this->getDoctrine()->getManager();
        $joueurs=$em->getRepository("UserBundle:Joueur")->findOneBy(array('nom'=>$joueur1->getNom()));
         if ($joueurs instanceof Joueur){
             return true;
         }
         else return false;

    }

//    public function findNomAction(Request $request){
//        $joueur=new Joueur();
//
//        $em=$this->getDoctrine()->getManager();
//     //   $joueur=$em->getRepository('UserBundle:Joueur:')->findAll();
//        $form=$this->createForm(RechercheJoueurForm::class,$joueur);
//        $form->handleRequest($request);
//        if($form->isValid()){
//            $nom=$joueur->getNom();
//            $joueurs=$em->getRepository('UserBundle:Joueur ')
//                ->RechercherSerieDQL($nom);
//        }
//        return $this->render('UserBundle:Joueur:rechercheNom.html.twig',
//            array('formrech'=>$form->createView(),'joueurs'=>$joueurs));
//    }


}
