<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\TestDopage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Testdopage controller.
 *
 */
class TestDopageController extends Controller
{
    /**
     * Lists all testDopage entities.
     *
     */
    public function indexAction()
    {
        $username =(string) $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $testDopages = $em->getRepository('UserBundle:TestDopage')->findAll();

        return $this->render('UserBundle:SuperAdmin:testDopage.html.twig', array(
            'testDopages' => $testDopages,

            'username'=>$username
        ));
    }

    /**
     * Creates a new testDopage entity.
     *
     */
    public function newAction(Request $request)
    {
        $username =(string) $this->getUser();
        $testDopage = new Testdopage();
        $form = $this->createForm('MyApp\UserBundle\Form\TestDopageType', $testDopage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($testDopage);
            $em->flush($testDopage);

            return $this->redirectToRoute('testdopage_show', array('id' => $testDopage->getId()));
        }

        return $this->render('UserBundle:testdopage:new.html.twig', array(
            'testDopage' => $testDopage,
            'form' => $form->createView(),

            'username'=>$username
        ));
    }

    /**
     * Finds and displays a testDopage entity.
     *
     */
    public function showAction(TestDopage $testDopage)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($testDopage);

        return $this->render('UserBundle:testdopage:show.html.twig', array(
            'testDopage' => $testDopage,
            'delete_form' => $deleteForm->createView(),
            'username'=>$username
        ));
    }

    /**
     * Displays a form to edit an existing testDopage entity.
     *
     */
    public function editAction(Request $request, TestDopage $testDopage)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($testDopage);
        $editForm = $this->createForm('MyApp\UserBundle\Form\TestDopageType', $testDopage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('testdopage_edit', array('id' => $testDopage->getId()));
        }

        return $this->render('UserBundle:testdopage:edit.html.twig', array(
            'testDopage' => $testDopage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=>$username
        ));
    }

    /**
     * Deletes a testDopage entity.
     *
     */
    public function deleteAction(Request $request, TestDopage $testDopage)
    {
        $username =(string) $this->getUser();
        $form = $this->createDeleteForm($testDopage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($testDopage);
            $em->flush($testDopage);
        }

        return $this->redirectToRoute('testdopage_index');
    }

    /**
     * Creates a form to delete a testDopage entity.
     *
     * @param TestDopage $testDopage The testDopage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TestDopage $testDopage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('testdopage_delete', array('id' => $testDopage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
