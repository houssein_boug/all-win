<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Stade;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Stade controller.
 *
 */
class StadeController extends Controller
{
    /**
     * Lists all stade entities.
     *
     */
    public function indexAction()
    {
        $username =(string) $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $stades = $em->getRepository('UserBundle:Stade')->findAll();

        return $this->render('UserBundle:SuperAdmin:stade.html.twig', array(
            'stades' => $stades,
            'username'=>$username
        ));
    }

    /**
     * Creates a new stade entity.
     *
     */
    public function newAction(Request $request)
    {
        $username =(string) $this->getUser();
        $stade = new Stade();
        $form = $this->createForm('MyApp\UserBundle\Form\StadeType', $stade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($stade);
            $em->flush($stade);

            return $this->redirectToRoute('stade_show', array('id' => $stade->getId()));
        }

        return $this->render('UserBundle:stade:new.html.twig', array(
            'stade' => $stade,
            'form' => $form->createView(),
            'username'=> $username
        ));
    }

    /**
     * Finds and displays a stade entity.
     *
     */
    public function showAction(Stade $stade)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($stade);

        return $this->render('UserBundle:stade:show.html.twig', array(
            'stade' => $stade,
            'delete_form' => $deleteForm->createView(),
            'username'=> $username


        ));
    }

    /**
     * Displays a form to edit an existing stade entity.
     *
     */
    public function editAction(Request $request, Stade $stade)
    {
        $username =(string) $this->getUser();
        $deleteForm = $this->createDeleteForm($stade);
        $editForm = $this->createForm('MyApp\UserBundle\Form\StadeType', $stade);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('stade_edit', array('id' => $stade->getId()));
        }

        return $this->render('UserBundle:stade:edit.html.twig', array(
            'stade' => $stade,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'username'=> $username
        ));

    }

    /**
     * Deletes a stade entity.
     *
     */
    public function deleteAction(Request $request, Stade $stade)
    {
        $username =(string) $this->getUser();
        $form = $this->createDeleteForm($stade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($stade);
            $em->flush($stade);
        }

        return $this->redirectToRoute('stade_index');
    }

    /**
     * Creates a form to delete a stade entity.
     *
     * @param Stade $stade The stade entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Stade $stade)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stade_delete', array('id' => $stade->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
