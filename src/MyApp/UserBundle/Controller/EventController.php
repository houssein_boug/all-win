<?php

namespace MyApp\UserBundle\Controller;

use MyApp\UserBundle\Entity\Evenement;
use MyApp\UserBundle\Form\EvenementForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


class EventController extends Controller
{
    public function afficherAction(Request $request)
    {

        $username =(string) $this->getUser();

        //$user = $this->getUser();
        //var_dump($user->getId());

       // $em = $this->getDoctrine()->getManager();
        //$user = $em->getRepository('UserBundle:User')->findOneBy(array('username'=>$username));

        //var_dump($user->getId());
        $event= new Evenement();
        $oldevent=new Evenement();



        $id = $request->get('id');
        if ($id != null)
        {
            //return $this->redirectToRoute('acceuil_SuperAdmin_homepage');
            $em=$this->getDoctrine()->getManager();
            $oldevent=$em->getRepository('UserBundle:Evenement')->find($id);
        }

        $Form=$this->createForm(EvenementForm::class,$event);
        $Form->handleRequest($request);

        if($Form->isValid())
        {

            if($event->getPhoto() != null)
            {

                $file = $event->getPhoto();
                $fileName ='';


                if ($oldevent->getPhoto() != null)
                {
                    $fileName= $oldevent->getPhoto();

                }
                else
                    {
                        // Generate a unique name for the file before saving it
                        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                    }

                $file->move($this->getParameter('events_photos_directory'), $fileName);
                $event->setPhoto($fileName);


            }
            else
            {
                $flag = $request->get('flag');
                if ( $flag != "")
                {
                    $event->setPhoto($oldevent->getPhoto() );
                }
                else{
                    $event->setPhoto(null);
                    if ($oldevent->getPhoto() != null)
                    {
                       $fs = new Filesystem();
                        $fs->remove($this->getParameter('events_photos_directory'), $oldevent->getPhoto());
                    }

                }
            }

            // Move the file to the directory where photoes are stored
           $oldevent->setPhoto($event->getPhoto());
           $oldevent->setAdresse($event->getAdresse());
           $oldevent->setTitre($event->getTitre());
           $oldevent->setDescription($event->getDescription());
           $oldevent->setDateDebut($event->getDateDebut());
           $oldevent->setDateFin($event->getDateFin());

            $em=$this->getDoctrine()->getManager();
            $em->persist($oldevent); //insert into
            $em->flush(); //push


            unset($event);
            unset($Form);
            $event = new Evenement();
            $Form = $this->createForm(EvenementForm::class,$event);
        }

        $events= $this->getDoctrine()
            ->getRepository('UserBundle:Evenement')
            ->findAll();
        return $this->render("UserBundle:SuperAdmin:gestionEvenement.html.twig",
            array('events'=>$events,'forms'=>$Form->createView(),'username'=>$username) );
     }

    public function supprimerAction(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $tab=json_decode($request->get('ids'));
        foreach ($tab as $id )
        {
        $evenement= $this->getDoctrine()->getRepository('UserBundle:Evenement')->find($id);
            $fs = new Filesystem();
            //$fs->remove($this->getParameter('events_photos_directory'),$evenement->getPhoto());
        $em->remove($evenement);
        $em->flush();
        }
        return new Response();
    }




        public function  ajoutCompteAction(Request $request)
        {
            $username =(string) $this->getUser();
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            echo " <script> alert('hi')</script>";
            if ($form->isValid() ) {

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                $r= $user->getRoles()[0];
                if ($r=="ROLE_CLIENT")
                return $this->redirectToRoute('acceuil_User_homepage');

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@User/Registration/registerAdmin.html.twig', array(
            'form' => $form->createView(),'username'=>$username
        ));
    }




}
