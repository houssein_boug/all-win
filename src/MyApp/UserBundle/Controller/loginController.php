<?php

namespace MyApp\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class loginController extends Controller
{

    public function acceuilSuperAdminAction()
    {
        $username=(string) $this->getUser();
        return $this->render('UserBundle:SuperAdmin:acceuilSuperAdmin.html.twig',array('username'=> $username));
    }
    public function acceuilAdminAction()
    {
        return $this->render('UserBundle:Admin:acceuilAdmin.html.twig');
    }
    public function acceuilUserAction()
    {
        return $this->render('UserBundle:User:acceuilUser.html.twig');
    }
    public function acceuilAction()
    {
        return $this->render('UserBundle:acceuil:acceuil.html.twig');
    }

}
