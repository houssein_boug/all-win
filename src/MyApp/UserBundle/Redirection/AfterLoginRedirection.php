<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 11/18/2016
 * Time: 3:37 AM
 */

namespace MyApp\UserBundle\Redirection;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router ;
    }

    /**
     * @param Resquest $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $roles = $token->getRoles();
        $rolesTab = array_map(function($role){return $role->getRole();}, $roles);

        if (in_array('ROLE_SUPER_ADMIN', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('acceuil_SuperAdmin_homepage'));
        elseif (in_array('ROLE_ADMIN', $rolesTab, true))
                $redirection = new RedirectResponse($this->router->generate('acceuil_Admin_homepage'));

            elseif (in_array('ROLE_USER', $rolesTab, true))
                $redirection = new RedirectResponse($this->router->generate('acceuil_User_homepage'));

                else $redirection=new RedirectResponse($this->router->generate('acceuil_homepage'));


        return $redirection;

    }





    }