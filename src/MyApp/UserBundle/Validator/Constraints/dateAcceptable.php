<?php
/**
 * Created by PhpStorm.
 * User: Khbou Omar
 * Date: 01/12/2016
 * Time: 03:18
 */

namespace MyApp\UserBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class dateAcceptable extends Constraint
{
        public $message=' the start date cannot pass the end date';
}