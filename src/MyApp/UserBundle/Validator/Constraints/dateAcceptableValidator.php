<?php
/**
 * Created by PhpStorm.
 * User: Khbou Omar
 * Date: 01/12/2016
 * Time: 03:23
 */

namespace MyApp\UserBundle\Validator\Constraints;


use Proxies\__CG__\MyApp\UserBundle\Entity\Joueur;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;


class dateAcceptableValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {

        // TODO: Implement validate() method.
        if ($value->g->getRang()==3){
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}