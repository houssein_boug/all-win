<?php

namespace MyApp\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paris
 *
 * @ORM\Table(name="paris")
 * @ORM\Entity
 */
class Paris
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="resultat", type="string", length=12, nullable=true)
     */
    private $resultat;




    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch1", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch1;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch2", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch2;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch3", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch3;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch4", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch4;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch5", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch5;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch6", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch6;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch7", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch7;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch8", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch8;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch9", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch9;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch10", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch10;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch11", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch11;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="\MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatch12", referencedColumnName="id",nullable=false)
     * })
     */
    private $idMatch12;


 //*************** mon getter and setter avec des variables dynamiques*******************//


    public function setIdMatch($i,$idMatch)
    {
        $attr="idMatch".$i;
        $this->$attr = $idMatch;
    }

    /**
     * @return \MyApp\UserBundle\Entity\Match
     */
    public function getIdMatch($i)
    {
        $attr="idMatch".$i;
       return  $this->$attr ;
    }
//*************************************************************************************//
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * @param string $resultat
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;
    }

    /**
     * @return \MyApp\UserBundle\Entity\Match
     */
    public function getIdMatch1()
    {
        return $this->idMatch1;
    }

    /**
     * @param \MyApp\UserBundle\Entity\Match $idMatch1
     */
    public function setIdMatch1($idMatch1)
    {
        $this->idMatch1 = $idMatch1;
    }

    /**
     * @return Match
     */
    public function getIdMatch2()
    {
        return $this->idMatch2;
    }

    /**
     * @param Match $idMatch2
     */
    public function setIdMatch2($idMatch2)
    {
        $this->idMatch2 = $idMatch2;
    }

    /**
     * @return Match
     */
    public function getIdMatch3()
    {
        return $this->idMatch3;
    }

    /**
     * @param Match $idMatch3
     */
    public function setIdMatch3($idMatch3)
    {
        $this->idMatch3 = $idMatch3;
    }

    /**
     * @return Match
     */
    public function getIdMatch4()
    {
        return $this->idMatch4;
    }

    /**
     * @param Match $idMatch4
     */
    public function setIdMatch4($idMatch4)
    {
        $this->idMatch4 = $idMatch4;
    }

    /**
     * @return Match
     */
    public function getIdMatch5()
    {
        return $this->idMatch5;
    }

    /**
     * @param Match $idMatch5
     */
    public function setIdMatch5($idMatch5)
    {
        $this->idMatch5 = $idMatch5;
    }

    /**
     * @return Match
     */
    public function getIdMatch6()
    {
        return $this->idMatch6;
    }

    /**
     * @param Match $idMatch6
     */
    public function setIdMatch6($idMatch6)
    {
        $this->idMatch6 = $idMatch6;
    }

    /**
     * @return Match
     */
    public function getIdMatch7()
    {
        return $this->idMatch7;
    }

    /**
     * @param Match $idMatch7
     */
    public function setIdMatch7($idMatch7)
    {
        $this->idMatch7 = $idMatch7;
    }

    /**
     * @return Match
     */
    public function getIdMatch8()
    {
        return $this->idMatch8;
    }

    /**
     * @param Match $idMatch8
     */
    public function setIdMatch8($idMatch8)
    {
        $this->idMatch8 = $idMatch8;
    }

    /**
     * @return Match
     */
    public function getIdMatch9()
    {
        return $this->idMatch9;
    }

    /**
     * @param Match $idMatch9
     */
    public function setIdMatch9($idMatch9)
    {
        $this->idMatch9 = $idMatch9;
    }

    /**
     * @return Match
     */
    public function getIdMatch10()
    {
        return $this->idMatch10;
    }

    /**
     * @param Match $idMatch10
     */
    public function setIdMatch10($idMatch10)
    {
        $this->idMatch10 = $idMatch10;
    }

    /**
     * @return Match
     */
    public function getIdMatch11()
    {
        return $this->idMatch11;
    }

    /**
     * @param Match $idMatch11
     */
    public function setIdMatch11($idMatch11)
    {
        $this->idMatch11 = $idMatch11;
    }

    /**
     * @return Match
     */
    public function getIdMatch12()
    {
        return $this->idMatch12;
    }

    /**
     * @param Match $idMatch12
     */
    public function setIdMatch12($idMatch12)
    {
        $this->idMatch12 = $idMatch12;
    }






}

