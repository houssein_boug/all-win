<?php

namespace MyApp\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Match
 *
 * @ORM\Table(name="`match`", uniqueConstraints={@ORM\UniqueConstraint(name="id_joueur1", columns={"id_joueur1"}), @ORM\UniqueConstraint(name="id_joueur2", columns={"id_joueur2"})}, indexes={@ORM\Index(name="id_stade", columns={"id_stade"}), @ORM\Index(name="id_arbitre", columns={"id_arbitre"})})
 * @ORM\Entity
 */
class Match
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_tickets_dispo", type="integer", nullable=false)
     */
    private $nbrTicketsDispo;

    /**
     * @var \MyApp\UserBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_joueur2", referencedColumnName="id")
     * })
     */
    private $idJoueur2;

    /**
     * @var \MyApp\UserBundle\Entity\Stade
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\Stade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_stade", referencedColumnName="id")
     * })
     */
    private $idStade;

    /**
     * @var \MyApp\UserBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_joueur1", referencedColumnName="id")
     * })
     */
    private $idJoueur1;

    /**
     * @var \MyApp\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_arbitre", referencedColumnName="id")
     * })
     */
    private $idArbitre;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getNbrTicketsDispo()
    {
        return $this->nbrTicketsDispo;
    }

    /**
     * @param int $nbrTicketsDispo
     */
    public function setNbrTicketsDispo($nbrTicketsDispo)
    {
        $this->nbrTicketsDispo = $nbrTicketsDispo;
    }

    /**
     * @return Joueur
     */
    public function getIdJoueur2()
    {
        return $this->idJoueur2;
    }

    /**
     * @param Joueur $idJoueur2
     */
    public function setIdJoueur2($idJoueur2)
    {
        $this->idJoueur2 = $idJoueur2;
    }

    /**
     * @return Stade
     */
    public function getIdStade()
    {
        return $this->idStade;
    }

    /**
     * @param Stade $idStade
     */
    public function setIdStade($idStade)
    {
        $this->idStade = $idStade;
    }

    /**
     * @return Joueur
     */
    public function getIdJoueur1()
    {
        return $this->idJoueur1;
    }

    /**
     * @param Joueur $idJoueur1
     */
    public function setIdJoueur1($idJoueur1)
    {
        $this->idJoueur1 = $idJoueur1;
    }

    /**
     * @return User
     */
    public function getIdArbitre()
    {
        return $this->idArbitre;
    }

    /**
     * @param User $idArbitre
     */
    public function setIdArbitre($idArbitre)
    {
        $this->idArbitre = $idArbitre;
    }


   public  function __toString()
    {
        return "".$this->getIdJoueur1()." VS ".$this->getIdJoueur2()." ".$this->getDate()->format('Y-m-d');

    }






}

