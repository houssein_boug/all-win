<?php

namespace MyApp\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompteRenduMatch
 *
 * @ORM\Table(name="compte_rendu_match", indexes={@ORM\Index(name="id_match", columns={"id_match"}), @ORM\Index(name="id_vainqueur", columns={"id_vainqueur"})})
 * @ORM\Entity
 */
class CompteRenduMatch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resultat", type="string", length=50, nullable=false)
     */
    private $resultat;

    /**
     * @var string
     *
     * @ORM\Column(name="detail_match", type="string", length=50, nullable=false)
     */
    private $detailMatch;

    /**
     * @var \MyApp\UserBundle\Entity\Match
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_match", referencedColumnName="id")
     * })
     */
    private $idMatch;

    /**
     * @var \MyApp\UserBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vainqueur", referencedColumnName="id")
     * })
     */
    private $idVainqueur;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * @param string $resultat
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;
    }

    /**
     * @return string
     */
    public function getDetailMatch()
    {
        return $this->detailMatch;
    }

    /**
     * @param string $detailMatch
     */
    public function setDetailMatch($detailMatch)
    {
        $this->detailMatch = $detailMatch;
    }

    /**
     * @return Match
     */
    public function getIdMatch()
    {
        return $this->idMatch;
    }

    /**
     * @param Match $idMatch
     */
    public function setIdMatch($idMatch)
    {
        $this->idMatch = $idMatch;
    }

    /**
     * @return Joueur
     */
    public function getIdVainqueur()
    {
        return $this->idVainqueur;
    }

    /**
     * @param Joueur $idVainqueur
     */
    public function setIdVainqueur($idVainqueur)
    {
        $this->idVainqueur = $idVainqueur;
    }


}

