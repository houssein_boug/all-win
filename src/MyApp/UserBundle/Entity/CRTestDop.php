<?php

namespace MyApp\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CRTestDop
 *
 * @ORM\Table(name="c_r_test_dop", indexes={@ORM\Index(name="id_test", columns={"id_test"})})
 * @ORM\Entity
 */
class CRTestDop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enonce", type="string", length=50, nullable=false)
     */
    private $enonce;

    /**
     * @var string
     *
     * @ORM\Column(name="destinataire", type="string", length=50, nullable=false)
     */
    private $destinataire;

    /**
     * @var string
     *
     * @ORM\Column(name="expediteur", type="string", length=50, nullable=false)
     */
    private $expediteur;

    /**
     * @var \MyApp\UserBundle\Entity\TestDopage
     *
     * @ORM\ManyToOne(targetEntity="MyApp\UserBundle\Entity\TestDopage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_test", referencedColumnName="id")
     * })
     */
    private $idTest;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEnonce()
    {
        return $this->enonce;
    }

    /**
     * @param string $enonce
     */
    public function setEnonce($enonce)
    {
        $this->enonce = $enonce;
    }

    /**
     * @return string
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }

    /**
     * @param string $destinataire
     */
    public function setDestinataire($destinataire)
    {
        $this->destinataire = $destinataire;
    }

    /**
     * @return string
     */
    public function getExpediteur()
    {
        return $this->expediteur;
    }

    /**
     * @param string $expediteur
     */
    public function setExpediteur($expediteur)
    {
        $this->expediteur = $expediteur;
    }

    /**
     * @return TestDopage
     */
    public function getIdTest()
    {
        return $this->idTest;
    }

    /**
     * @param TestDopage $idTest
     */
    public function setIdTest($idTest)
    {
        $this->idTest = $idTest;
    }

    function __toString()
    {
        return $this->getDestinataire();
    }




}

