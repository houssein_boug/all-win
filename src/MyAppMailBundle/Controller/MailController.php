<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 01/12/2016
 * Time: 21:46
 */

namespace MyAppMailBundle\Controller;


use MyAppMailBundle\Entity\Mail;
use MyAppMailBundle\Form\MailForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Swift_Message;
use Symfony\Component\HttpFoundation\Response;

class MailController extends  Controller
{
    public function indexAction(Request $request)
    {$username =(string) $this->getUser();
    $mail = new Mail();
    $form = $this->createForm( MailForm::class, $mail);
    $form->handleRequest($request);
    if ($form->isValid()) {
        $message = \ Swift_Message::newInstance()
            ->setSubject('Accusé de réception')
            ->setFrom('espritplus2016@gmail.com')
            ->setTo($mail->getEmail())
            ->setBody($this->renderView('MyAppMailBundle::mail.html.twig', array('nom' => $mail->getNom(), 'prenom' => $mail->getPrenom(), 'text'=>$mail->getText())), 'text/html');
        $this->get('mailer')->send($message);
        return $this->redirect($this->generateUrl('my_app_mail_accuse'));
    }
    return $this->render('MyAppMailBundle::new.html.twig', array('form' => $form->createView(),'nom' => $mail->getNom(), 'prenom' => $mail->getPrenom(),'username'=> $username));
    }

    public function successAction()
    {
        return new Response("email envoyé avec succès, Merci de vérifier votre adresse mail.");
    }







}