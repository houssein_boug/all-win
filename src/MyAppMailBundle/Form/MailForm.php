<?php

namespace MyAppMailBundle\Form;

use Doctrine\DBAL\Types\IntegerType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {$builder
        ->add('nom', TextType::class)
        ->add('prenom', TextType::class)
        ->add('tel', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class)
        ->add('email',EmailType::class)
        ->add('text', TextareaType::class)
        ->add('valider', SubmitType::class) ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {


    }

    public function getName()
    {
        return 'my_app_mail_bundle_mail_form';
    }
}
